#include "Rigidbody.h"
#include "glm/ext.hpp"
#include <iostream>
//#include "GameDefines.h"

Rigidbody::Rigidbody(ShapeType shapeID)
{
	this->shapeID = shapeID;
}

Rigidbody::Rigidbody(ShapeType shapeID, glm::vec2 position, glm::vec2 velocity, float orientation, float mass, bool isStatic)
{
	this->shapeID = shapeID;
	this->position = position;
	this->velocity = velocity;
	this->orientation = orientation;
	this->mass = mass;
	this->isStatic = isStatic;
	this->elasticity = 0.8f;	//default value
}

Rigidbody::~Rigidbody()
{	
	//for (auto contactPoint : contactPoints)
	//{
	//	delete contactPoint;
	//}
}

void Rigidbody::FixedUpdate(glm::vec2 gravity, float timeStep)
{
	velocity *= (1 - damping);	//simulates air reistance
	//Add updated vector to actor's position to get new position
	position += velocity * timeStep;

	//By multiplying by object's mass, gravity acts as pure acceleration; you'll divide by mass again inside ApplyForce()
	//We apply gravity after updating position in this simulation to avoid a 'quicksanding' effect.
	//If you apply gravity first, the object's velocity when it gets applied to position is one frame's
	//worth of acceleration due to gravity. This velocity doesn't accumulate because it gets zeroed every frame
	//but it will move the object a small amount each frame.

	ApplyForce(gravity * mass * timeStep);	//apply gravity (without applying force to a contactpoint)

	////Establish thresholds under which objects stop moving/rotating
	////CURRENTLY, THE VELOCITY NEVER DROPS BELOW THIS
	//if (length(velocity) < MIN_LINEAR_THRESHOLD)
	//{
	//	if (length(velocity) < length(gravity) * linearDrag * timeStep)
	//	{
	//		velocity = glm::vec2(0, 0);
	//	}
	//}

	//DEBUGGING
	//DrawContactPoints();
}

//Apply Newton's second law; modifies the velocity of the object according to its mass and force applied
void Rigidbody::ApplyForce(glm::vec2 force)
{
	if (!IsStatic())
	{
		//Use GetMass() and GetMoment() here in case we ever need them to do something more than just return their values 
		//Calculate the object's acceleration
		glm::vec2 acceleration = force / GetMass();
		//Add acceleration to object's velocity
		velocity += acceleration;
	}
}

//Simulates one actor 'pushing' another
void Rigidbody::ApplyForceToActor(Rigidbody* actor, glm::vec2 force)
{
	//Apply input force to input actor (i.e. the actor this is hitting)
	actor->ApplyForce(force);

	//Apply opposite force to this actor
	ApplyForce(-force);
}

//NOTE: You should update this to check if one of the collision objects is static (or kinematic) and, if so, uses an infinite mass inthose calculations
void Rigidbody::ResolveCollision(Rigidbody* actor, glm::vec2* collisionNormal)
{
	//Calculate the collision normal (normallized difference in position -- okay for spheres
	glm::vec2 normal = collisionNormal ? *collisionNormal : glm::normalize(actor->GetPosition() - this->GetPosition());
	//glm::vec2 normal = glm::normalize(actor->GetPosition() - position);
	
	//Calculate relative velocity between two objects -- improve by determining total velocity (linear + rotational velocity)
	glm::vec2 relativeVelocity = actor->GetVelocity() - this->GetVelocity();

	//elasticity of 1 means no energy will be lost during collision
	//You could define values for each object and combine them to determine a more phisically convincing value
	glm::vec2 force;
	
	if (this->IsStatic() && !(actor->IsStatic()))
	{
		float j = glm::dot(-(1 + this->elasticity) * (relativeVelocity), normal) / (1 / actor->GetMass());
		force = normal * j;
		actor->ApplyForce(force);
	}
	else if (actor->IsStatic() && !(this->IsStatic()))
	{
		float j = glm::dot(-(1 + this->elasticity) * (relativeVelocity), normal) / ((1 / mass));
		force = normal * j;
		ApplyForce(-force);
	}
	else
	{
		float j = glm::dot(-(1 + this->elasticity) * (relativeVelocity), normal) / ((1 / mass) + (1 / actor->GetMass()));
		force = normal * j;
		ApplyForce(-force);
		actor->ApplyForce(force);
	}	
	//DEBUG CREATE CONTACT POINTS
	//ContactPoint* contactPoint = new ContactPoint(contact);
	//AddContactPoint(contactPoint);
}

void Rigidbody::AddToPosition(glm::vec2 offset)
{
	position += offset;
}

//Calculates linear kinetic energy
float Rigidbody::GetKineticEnergy()
{
	float velMagnitude = glm::length(velocity);
	float kineticEnergy = 0.5f * mass * pow(velMagnitude, 2);
	return kineticEnergy;
}

//void Rigidbody::AddContactPoint(ContactPoint* contactPoint)
//{
//	contactPoints.push_back(contactPoint);
//}

//void Rigidbody::DrawContactPoints()
//{
//	for (int i = 0; i < contactPoints.size(); i++)
//	{
//		aie::Gizmos::add2DCircle(contactPoints[i]->GetPosition(), 2, 24, glm::vec4(1, 1, 1, 1));
//	}
//}

////Translates a position in the rigidbody's coordinate system to world coordinates
//void Rigidbody::ToWorld(glm::vec2 position)
//{
//
//}