#pragma once
#include "glm/ext.hpp"

glm::vec4 COLOUR_WHITE = { 1.0f, 1.0f, 1.0f, 1.0f };
glm::vec4 COLOUR_RED = { 1.0f, 0.1f, 0.3f, 1.0f };
glm::vec4 COLOUR_GREEN = { 0.2f, 1.0f, 0.2f, 1.0f };
glm::vec4 COLOUR_BLUE = { 0.2f, 0.1f, 1.0f, 1.0f };
glm::vec4 COLOUR_BROWN = { 0.8f, 0.6f, 0.1f, 1.0f };

float MIN_LINEAR_THRESHOLD = 0.1f;
float MIN_ANGULAR_THRESHOLD = 0.01f;